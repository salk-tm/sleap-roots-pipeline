import glob
import json
import os
from pathlib import Path
import re
import shutil
import sys
import subprocess
import zipfile

import h5py
import imageio as iio
import pandas as pd
import sleap
from sleap_roots import Series
import numpy as np


batch_size = 4  # number of images to process at once


def main(input_dir, output_dir):
    """Run the SLEAP roots pipeline.

    Args:
        input_dir (str): Contains scans.csv, params.json, primary.zip, and lateral.zip.
    """
    scans_csv = os.path.join(input_dir, "scans.csv")
    params_json = os.path.join(input_dir, "params.json")

    # load scans_csv as dataframe
    scans_df = pd.read_csv(scans_csv)

    # load params_json as dict
    with open(params_json) as f:
        params = json.load(f)

    # set permissible names
    model_names = ["primary", "lateral"]

    # unzip the models
    for model_path in glob.glob(os.path.join(input_dir, "*.zip")):
        # get model name and dir
        model_name = os.path.splitext(os.path.basename(model_path))[0]
        assert(model_name in model_names)
        model_dir = os.path.join(input_dir, model_name)
        # check if model is already unzipped
        if os.path.isdir(model_dir):
            pass
        else:
            with zipfile.ZipFile(model_path, "r") as zip_ref:
                zip_ref.extractall(model_dir)
    
    # construct model dirs
    model_dirs = [os.path.join(input_dir, model_name) for model_name in model_names]
    
    # convert scan directories to h5 files
    h5_paths = []
    image_paths = []
    for scan_path in scans_df["scan_path"]:
        h5_path = convert_img_folder_to_h5(scan_path)
        h5_paths.append(h5_path)
        scan_image_paths = natural_sort(list(Path(scan_path).glob("*.png")))
        image_paths.append(scan_image_paths)

    # run each model on each scan
    for (model_dir, model_name) in zip(model_dirs, model_names):

        # load model
        model = sleap.load_model(
            model_dir,
            batch_size=batch_size,
            progress_reporting="none"
        )

        # run model on each scan
        for h5_path in h5_paths:
            get_predictions(h5_path, model_name, model)

    # get pipeline class
    TraitPipeline = get_pipeline_class(params)
    pipeline = TraitPipeline()

    # run trait computations
    image_trait_dfs = []
    series_trait_dfs = []
    for i, (h5_path, scan_id) in enumerate(zip(h5_paths, scans_df["scan_id"])):

        # load series of images (scan)
        series = Series.load(h5_path, primary_name="primary", lateral_name="lateral")

        # compute image traits
        image_traits = pipeline.compute_plant_traits(series, write_csv=False)
        # add scan_id column at the beginning
        image_traits = pd.concat([pd.DataFrame({"scan_id": [scan_id for _ in range(len(image_traits))]}), image_traits], axis=1)
        # extract frame numbers from paths (e.g. "scan_3/1.png" -> 1)
        frame_numbers = [int(os.path.splitext(os.path.basename(image_path))[0]) for image_path in image_paths[i]]
        # rename frame_idx column to frame_number and replace with frame numbers
        image_traits = image_traits.rename(columns={"frame_idx": "frame_number"})
        image_traits["frame_number"] = frame_numbers
        # append to list of image trait dfs
        image_trait_dfs.append(image_traits)

        # compute plant ("batch") traits
        series_traits = pipeline.compute_batch_traits([series])
        # add scan_id column at the beginning
        series_traits = pd.concat([pd.DataFrame({"scan_id": [scan_id for _ in range(len(series_traits))]}), series_traits], axis=1)
        # append to list of series trait dfs
        series_trait_dfs.append(series_traits)
    
    # concatenate dfs
    image_trait_df = pd.concat(image_trait_dfs, ignore_index=True)
    series_trait_df = pd.concat(series_trait_dfs, ignore_index=True)

    # change "plant_name" columns to "plant_qr_code"
    image_trait_df = image_trait_df.rename(columns={"plant_name": "plant_qr_code"})
    series_trait_df = series_trait_df.rename(columns={"plant_name": "plant_qr_code"})    

    # save image trait df
    image_trait_df.to_csv(os.path.join(output_dir, "image_traits_csv"), index=False)

    # save series trait df
    series_trait_df.to_csv(os.path.join(output_dir, "scan_traits_csv"), index=False)

    # delete model dirs
    for model_dir in model_dirs:
        shutil.rmtree(model_dir)

def get_predictions(filename, subtype, predictor, overwrite=False):
    preds_path = f"{filename[:-3]}.{subtype}.predictions.slp"
    if Path(preds_path).exists() and not overwrite:
        pass
    else:
        video = sleap.load_video(filename, dataset="vol", channels_first=False)
        predictions = predictor.predict(video)
        predictions.save(preds_path)

def natural_sort(l):
    """https://stackoverflow.com/a/4836734"""
    l = [x.as_posix() if isinstance(x, Path) else x for x in l]
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


def flatten(numpy_array):
    """Flatten a numpy array to 1 channel."""
    if numpy_array.ndim == 2:
        return numpy_array
    elif numpy_array.ndim == 3:
        return numpy_array[:, :, 0]
    else:
        raise ValueError("Input array must be 2D or 3D.")


def convert_img_folder_to_h5(img_folder, overwrite=False):
    """Convert an image folder to an HDF5 file.
    
    Args:
        img_folder: Path to a folder filled with PNGs named sequentially.
        overwrite: If True, save the HDF5 file even if it already exists.
        
    Notes:
        This will save a file with the same filename as the image folder, but
        with a .h5 extension. The resulting file will contain a dataset named
        "vol" with shape (slices, height, width, 1).
    """
    img_folder = Path(img_folder).as_posix()
    h5_name = f"{img_folder}.h5"
    if not overwrite and os.path.exists(h5_name):
        return h5_name
    p = Path(img_folder)
    img_paths = natural_sort(list(p.glob("*.png")))
    vol = np.stack([flatten(iio.imread(p)) for p in img_paths], axis=0)  # (slices, height, width)
    with h5py.File(h5_name, "w") as f:
        data = np.expand_dims(vol, axis=-1)
        ds = f.create_dataset(
            "vol",
            data=data,  # (slices, height, width, 1)
            compression=1
        )
    return h5_name

def get_pipeline_class(params):
    """Get the pipeline class from the params dict."""
    # get pipeline name
    from sleap_roots import (DicotPipeline, YoungerMonocotPipeline)

    pipeline_name = params["pipeline_class"]
    pipeline_classes = {
        "DicotPipeline": DicotPipeline,
        "YoungerMonocotPipeline": YoungerMonocotPipeline,
    }
    return pipeline_classes[pipeline_name]

if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
